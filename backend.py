#################################################
## ------------------------------------------- ##
## IMPORTANTE: Host se abre en 0.0.0.0, no en  ##
## localhost, aunque JS se conecte a localhost ##
## ------------------------------------------- ##
#################################################

from flask import Flask, request, render_template ## Importamos Flask
from flask_cors import CORS ## No sé que garcha hace esto pero sin eso no anda

app = Flask(__name__) ## Declaramos la API bajo la variable app
CORS(app) ## De nuevo, ni idea que pija hace pero le gusta que esté

@app.route('/nombre', methods=['POST']) 
## Declaramos que la proxima función se va a ejecutar cuando se le haga un request a 0.0.0.0:80/nombre

def recibirNombre():
    print (request.form)
    nom = request.form["nom"] ## Guardamos el nombre que se recibió 
    ape = request.form["ape"] ## Guardamos el apellido que se recibió 
    devolver = "Buenardas, " + nom + " " + ape 
    return (devolver) ## Devolvemos "devolver"


app.run(host='0.0.0.0', port='8000') ## Abrimos la API en http://0.0.0.0:8000/