/////////////////////////////////////////////////
/* ------------------------------------------- */
/* IMPORTANTE: Host se abre en 0.0.0.0, no en  */
/* localhost, aunque JS se conecte a localhost */
/* ------------------------------------------- */
/////////////////////////////////////////////////

var url = "http://localhost:8000/" //Esta es la url principal en la que está abierta la API

function enviarDatos() {
    var nom = document.getElementById("nombre").value //Lee el value del elemento con el ID "nombre"
    var ape = document.getElementById("apellido").value //Lee el value del elemento con el ID "nombre"
    var xhr = new XMLHttpRequest(); //Genera una variable para guardar el request
    var dir = url + "nombre"; //Define que la direccion de la API será http://localhost:8000/nombre
    xhr.open("POST", dir, true); //Abre el request en "dir" del tipo POST (Enviará información)
    var data = new FormData(); //Genera un formulario vacío
    data.append("nom", nom); //Le agrega el nombre al formulario
    data.append("ape", ape); //Le agrega el apellido al formulario
    xhr.send(data); //Envia la información del formulario a la API
    xhr.onreadystatechange = function() { //Funcion que se ejecuta cuando se recibe información
        if (xhr.readyState === 4 && xhr.status === 200) { //Chequea que no tenga errores (en conección, no en el codigo de python)
            var recibido = xhr.responseText; //Guarda en una variable el string recibido
            document.getElementById("recibido").innerHTML = recibido; //Escribe la variable recibido dentro del elemento con el ID "recibido"
        };
    };
};